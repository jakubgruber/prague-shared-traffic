const express = require('express');
const router = express.Router();

const bingMapsService = require('../service/bingMapsService');

const Vehicle = require('../model/vehicleModel');
const Route = require('../model/routeModel');

/**
 * @swagger
 * /routes/:
 *   get:
 *     description: Returns all taken routes
 *     tags: [Routes]
 *     responses:
 *       200:
 *          description: An array of taken routes
 *          schema:
 *              type: array
 *              items:
 *                  $ref: "#/definitions/Route"
 */
router.get('/', async (req, res) => {
    let allRoutes = await Route.find({}).lean();
    console.log('Routes found: ' + allRoutes.length);
    res.send(allRoutes);
});

/**
 * @swagger
 * /routes/{id}:
 *  delete:
 *     description: Deletes saved route by id
 *     tags: [Routes]
 *     parameters:
 *      - in: path
 *        name: id
 *        type: string
 *        description: Id of a route to delete
 *        required: true
 *     responses:
 *       200:
 *          description: The route was deleted
 */
router.delete('/:id', async (req, res) => {
    await Route.findByIdAndDelete(req.params.id);
    res.sendStatus(200);
});

/**
 * @swagger
 *
 * /routes/compute:
 *   post:
 *     description: Computes route by given coordinates and stores both route and vehicle
 *     tags: [Routes]
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: route to compute
 *         in:  body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/RouteToCompute'
 *     responses:
 *       200:
 *         description: computed route
 *         schema:
 *           $ref: '#/definitions/ComputedRoute'
 */
router.post('/compute', async (req, res) => {
    const {vehicle, from} = req.body;

    const to = vehicle.coordinates.lat + ',' + vehicle.coordinates.lng;
    const fromCoords = from.lat + ',' + from.lng;

    const computedRoute = await bingMapsService.computeRoute(fromCoords, to);
    computedRoute.vehicle = vehicle;
    computedRoute.from = from;

    let route = new Route({
        to: vehicle.coordinates,
        from: from,
        distance: computedRoute.distance.toFixed(2),
        duration: computedRoute.duration,
    });

    console.log('About to save route: ' + route);
    await route.save();
    console.log('Route saved: ' + route.id);

    await Vehicle.updateOne(
        {
            name: vehicle.name,
            company: vehicle.company
        },
        vehicle,
        {
            upsert: true
        });

    res.send(computedRoute);
});


/**
 * @swagger
 * /routes/statistics:
 *   get:
 *     description: Returns statistics of all taken routes
 *     tags: [Routes]
 *     responses:
 *       200:
 *          description: An object of routes statistics
 *          schema:
 *              $ref: "#/definitions/RouteStats"
 */
router.get('/statistics', async (req, res) => {
    let stats = await Route.aggregate(
        [
            {
                $group: {
                    _id: 0,
                    totalDistance: {$sum: '$distance'},
                    totalDuration: {$sum: '$duration'},
                    lastTakenDate: {$max: '$dateTaken'}
                }
            }
        ]);
    if(!stats || stats.length === 0) {
        res.send([]);
        return
    }
    delete stats[0]._id;

    stats[0].totalDistance = stats[0].totalDistance.toFixed(2);

        res.send(stats[0])
});

module.exports = router;