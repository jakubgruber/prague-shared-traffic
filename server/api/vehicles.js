const express = require('express');
const router = express.Router();

let golemioService = require('../service/golemioService');

const Vehicle = require('../model/vehicleModel');

/**
 * @swagger
 * /vehicles:
 *   get:
 *     description: Returns all used vehicles
 *     tags: [Vehicles]
 *     responses:
 *       200:
 *          description: An array of all used vehicles
 *          schema:
 *              type: array
 *              items:
 *                  allOf:
 *                      - $ref: '#/definitions/Vehicle'
 *                      - type: object
 *                        properties:
 *                          _id:
 *                             type: string
 */
router.get('/', async (req, res) => {
    let allVehicles = await Vehicle.find({}).lean();
    console.log('Vehicles found: ' + allVehicles.length);
    res.send(allVehicles);
});

/**
 * @swagger
 * /vehicles/around:
 *   get:
 *     description: Returns all available vehicles around
 *     tags: [Vehicles]
 *     parameters:
 *      - in: query
 *        name: latlng
 *        type: string
 *        description: Comma separated latitude and longitude values
 *        required: true
 *     responses:
 *       200:
 *          description: An array of all shared vehicles
 *          schema:
 *              type: array
 *              items:
 *                  $ref: "#/definitions/Vehicle"
 */
router.get('/around', async (req, res) => {
    let latlng = req.query.latlng;
    let response = await golemioService.getAllSharedBikes(latlng);
    res.send(response);
});

/**
 * @swagger
 * /vehicles/{id}:
 *  delete:
 *     description: Deletes saved vehicles by id
 *     tags: [Vehicles]
 *     parameters:
 *      - in: path
 *        name: id
 *        type: string
 *        description: Id of a vehicle to delete
 *        required: true
 *     responses:
 *       200:
 *          description: The vehicle was deleted
 */
router.delete('/:id', async (req, res) => {
    await Vehicle.findByIdAndDelete(req.params.id);
    res.sendStatus(200);
});

/**
 * @swagger
 * /vehicles/statistics:
 *   get:
 *     description: Returns statistics of vehicles - number of rides per company
 *     tags: [Vehicles]
 *     responses:
 *       200:
 *          description: An object of vehicle statistics
 *          schema:
 *              type: array
 *              items:
 *                  $ref: "#/definitions/VehicleStats"
 */
router.get('/statistics', async (req, res) => {
    let stats = await Vehicle.aggregate(
        [
            {
                $group: {
                    _id: '$company',
                    noOfRides: { $sum: 1 },
                }
            },
            { $project: {
                    _id: 0,
                    company: "$_id",
                    noOfRides: 1,
                }
            }
        ]);

    console.log('Aggregation: ');
    console.log(stats);

    res.send(stats)


});

module.exports = router;
