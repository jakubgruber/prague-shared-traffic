const express = require('express');
const router = express.Router();

const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');

const swaggerDefinition = {
    info: {
        openapi: '3.0.0',
        title: 'Prague Shared Traffic',
        version: '1.0.0',
        description: 'API exposing Prague data regarding shared traffic',
    },
    host: `prague-shared-traffic.herokuapp.com`,
};

const options = {
    swaggerDefinition,
    // Note that this path is relative to the current directory from which the Node.js is ran, not the application itself.
    apis: ['./server/api/*.js', './server/api/swagger-definitions.yaml']
};

const swaggerSpec = swaggerJSDoc(options);

router.use('/', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

module.exports = router;