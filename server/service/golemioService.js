const axios = require('axios');

class GolemioService {

    constructor() {
        this.apiKey = process.env.GOLEMIO_API_KEY;
        this.baseUrl = process.env.GOLEMIO_URL;
    }

    async getAllSharedBikes(latlng) {
        let response = await axios.get(this.baseUrl + '/sharedbikes', {
            headers: {
                'Content-Type': 'application/json; charset=utf-8',
                'x-access-token': this.apiKey
            },
            params: {
                latlng: latlng
            }
        });

        return response.data.features.map((feature) => ({
            coordinates: {
                lat: feature.geometry.coordinates[1],
                lng: feature.geometry.coordinates[0]
            },
            company: feature.properties.company.name,
            name: feature.properties.name,
            type: feature.properties.type.description
        }));
    }

}

module.exports = new GolemioService();