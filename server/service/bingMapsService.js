const axios = require('axios');

class BingMapsService {

    constructor() {
        this.apiKey = process.env.BING_MAPS_API_KEY;
        this.baseUrl = process.env.BING_MAPS_URL;
    }

    async computeRoute(from, to) {
        let response = await axios.get(this.baseUrl + '/Routes/Walking', {
            params: {
                'wp.0': from,
                'wp.1': to,
                'optmz': 'distance',
                'key': this.apiKey
            }
        });

        const itineraryItems = response.data.resourceSets[0].resources[0].routeLegs[0].itineraryItems.map((item) => ({
            text: item.instruction.text,
            place: {
                lat: item.maneuverPoint.coordinates[0],
                lng: item.maneuverPoint.coordinates[1]
            }
        }));

        return {
            distance: response.data.resourceSets[0].resources[0].travelDistance,
            duration: Math.floor(response.data.resourceSets[0].resources[0].travelDuration / 60),
            itineraryItems: itineraryItems
        };
    }

}

module.exports = new BingMapsService();