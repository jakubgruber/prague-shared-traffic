const mongoose = require('./dbClient');

const vehicleSchema = mongoose.Schema({
    coordinates: {
        lat: Number,
        lng: Number,
    },
    company: String,
    name: String,
    type: String
}, {versionKey: false});

module.exports = mongoose.model('vehicle', vehicleSchema);