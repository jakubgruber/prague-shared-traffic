const mongoose = require('mongoose');

mongoose.connect(process.env.MONGO_URL, {
    useNewUrlParser: true,
});

const db = mongoose.connection;
db.on('error', () => {
    console.log('Error occurred from the database');
});
db.once('open', () => {
    console.log('Successfully opened the database');
});

module.exports = mongoose;
