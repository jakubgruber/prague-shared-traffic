const mongoose = require('./dbClient');

const routeSchema = mongoose.Schema({
    from: {
        lat: Number,
        lng: Number,
    },
    to: {
        lat: Number,
        lng: Number,
    },
    dateTaken: { type: Date, default: Date.now },
    distance: Number,
    duration: Number
}, { versionKey: false });

module.exports = mongoose.model('route', routeSchema);
