const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const path = require('path');

require('dotenv').config();

const vehiclesRoutes = require('./server/api/vehicles');
const routesRoutes = require('./server/api/routes');
const docsRoutes = require('./server/api/docs');

const app = express();
const port = process.env.PORT || 8080;

app.use(bodyParser.json({extended: true}));
app.use(cors());

// Serve static files from the React app
app.use(express.static(path.join(__dirname, 'client/build')));

app.use('/vehicles', vehiclesRoutes);
app.use('/routes', routesRoutes);
app.use('/docs', docsRoutes);

// The "catchall" handler: for any request that doesn't
// match one above, send back React's index.html file.
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname + '/client/build/index.html'));
});

app.listen(port, () => console.log(`Server listening on port ${port}!`));

module.exports = app;