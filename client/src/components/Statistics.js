import React from 'react';
import {RoutesStatistics} from "./routes/RoutesStatistics";
import {VehiclesStatistics} from "./vehicles/VehiclesStatistics";

export class Statistics extends React.Component {

    render() {
        return (
            <div className="mx-auto card w-75 my-3 check-statistics">
                <div className="card-header">
                    Statistics
                </div>
                <div className="card-body row">
                    <VehiclesStatistics/>
                    <RoutesStatistics/>
                </div>
            </div>
        );
    }

}