import React from 'react';
import {MapInfoProvider} from "../../contextProvider";

export class VehiclesHistory extends React.Component {

    deleteVehicleOnClick = (onDeleteVehicle, id) => {
        onDeleteVehicle(id);
    };

    render() {
        return (
            <MapInfoProvider.Consumer>
                {({vehicleHistory, onDeleteVehicle}) => {
                    if(!vehicleHistory) vehicleHistory = [];
                    return (
                        <div className="col">
                            <h5 className="card-title">Vehicles</h5>
                            <ul className="list-group list-group-flush">
                                {
                                    vehicleHistory.map((vehicle, key) => {
                                        return <li className="list-group-item" key={key}>
                                            <div className="card-body">
                                                <button type="button" className="close delete-history" aria-label="Close" onClick={e => this.deleteVehicleOnClick(onDeleteVehicle, vehicle._id)}>
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                <h6 className="card-subtitle mb-2 text-muted">{vehicle.name}</h6>
                                                <p className="card-text">{vehicle.company}'s {vehicle.type}.</p>
                                            </div>
                                        </li>
                                    })
                                }
                            </ul>
                        </div>
                    );
                }}
            </MapInfoProvider.Consumer>
        );
    }

}