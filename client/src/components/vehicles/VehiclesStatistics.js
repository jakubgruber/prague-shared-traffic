import React from 'react';
import {MapInfoProvider} from "../../contextProvider";

export class VehiclesStatistics extends React.Component {

    render() {
        return (
            <MapInfoProvider.Consumer>
                {({vehicleStats}) => {
                    if (!vehicleStats) vehicleStats = [];
                    return (
                        <div className="col">
                            <h5 className="card-title">Vehicles statistics</h5>
                            <ul className="list-group list-group-flush">
                                {
                                    vehicleStats.map((stat, key) => {
                                        return <li className="list-group-item" key={key}>
                                            <div className="card-body">
                                                <p className="card-text">{stat.noOfRides} rides taken with vehicles
                                                    from {stat.company}</p>
                                            </div>
                                        </li>
                                    })
                                }
                            </ul>
                        </div>
                    );
                }}
            </MapInfoProvider.Consumer>);
    }
}