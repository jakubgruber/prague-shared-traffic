import React from 'react';
import GoogleMapReact from 'google-map-react';
import Marker from "./Marker";
import {MapInfoProvider} from "../contextProvider";
import {SharedTrafficService} from "../service/SharedTrafficService";

export class TrafficMap extends React.Component {

    constructor(props) {
        super(props);

        this.service = new SharedTrafficService();

        this.state = {

            vehicles: [],
            chosenVehicle: {},
            computationEnabled: false
        };
    }

    static defaultProps = {
        center: {
            // start in Prague
            lat: 50.08804,
            lng: 14.42076
        },
        zoom: 13
    };

    configureMap = (onMapLoaded, map, maps) => {
        this.setState({
            map: map,
            maps: maps
        });

        if ("geolocation" in navigator) {
            navigator.geolocation.getCurrentPosition((position => {
                this.setState({
                    userPosition: {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    }
                });
                this.findVehicles(this.state.userPosition.lat, this.state.userPosition.lng);
            }))
        }

        onMapLoaded(map, maps);
    };

    onMapClicked = (lat, lng, resetRouteInfo) => {
        this.setState({
            userPosition: {
                lat: lat,
                lng: lng
            }
        });

        this.findVehicles(lat, lng);

        resetRouteInfo();
    };

    findVehicles = async (lat, lng) => {
        let vehicles = await this.service.getVehiclesAround(lat, lng);
        this.setState({
            vehicles: vehicles
        })
    };

    compute = (computeRoute) => {
        computeRoute(this.state.chosenVehicle, this.state.userPosition);
    };

    onVehicleMarkerClicked = (vehicleMarker, resetRouteInfo) => {
        this.setState({
            chosenVehicle: vehicleMarker.vehicle,
            computationEnabled: true
        });

        resetRouteInfo();
    };

    render() {
        return (
            // Important! Always set the container height explicitly
            <MapInfoProvider.Consumer>
                {({computeRoute, onMapLoaded, resetRouteInfo}) => (
                    <div className="m-5 map-location">
                        <div style={{height: '70vh'}}>
                            <GoogleMapReact
                                bootstrapURLKeys={{key: process.env.REACT_APP_GOOGLE_MAPS_API_KEY}}
                                yesIWantToUseGoogleMapApiInternals={true}
                                defaultCenter={this.props.center}
                                defaultZoom={this.props.zoom}
                                onClick={({x, y, lat, lng, event}) => this.onMapClicked(lat, lng, resetRouteInfo)}
                                center={this.state.userPosition}
                                onGoogleApiLoaded={({map, maps}) => this.configureMap(onMapLoaded, map, maps)}>
                                {this.state.userPosition && <Marker
                                    lat={this.state.userPosition.lat}
                                    lng={this.state.userPosition.lng}
                                    name="Current Position"
                                    color="blue"/>
                                }
                                {this.state.vehicles.map((v, key) => {
                                        let color;
                                        if (v.coordinates === this.state.chosenVehicle.coordinates) {
                                            color = 'green'
                                        } else {
                                            color = 'red'
                                        }

                                        return <Marker
                                            key={key}
                                            lat={v.coordinates.lat}
                                            lng={v.coordinates.lng}
                                            name={v.name}
                                            vehicle={v}
                                            color={color}
                                            onMarkerClicked={(vehicleMarker) => this.onVehicleMarkerClicked(vehicleMarker, resetRouteInfo)}/>
                                    }
                                )}
                            </GoogleMapReact>
                        </div>
                        <div className="text-center p-2">
                            <button type="button" className="btn btn-primary btn-lg goforit-button"
                                    onClick={(e) => this.compute(computeRoute)}
                                    disabled={!this.state.computationEnabled}
                            >Go for it!
                            </button>
                        </div>
                    </div>
                )}

            </MapInfoProvider.Consumer>
        );
    }

}