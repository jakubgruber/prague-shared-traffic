import React from 'react';
import './Marker.css'

export default class Marker extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <div
                    className="pin bounce"
                    style={{backgroundColor: this.props.color, cursor: 'pointer'}}
                    title={this.props.name}
                    onClick={(e) => {
                        this.props.onMarkerClicked(this.props);
                        e.preventDefault();
                    }}
                />
            </div>
        );
    }
}