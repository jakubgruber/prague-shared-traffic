import React from 'react';

import {VehiclesHistory} from "./vehicles/VehiclesHistory";
import {RoutesHistory} from "./routes/RoutesHistory";

export class History extends React.Component {

    render() {
        return (
            <div className="mx-auto card w-75 my-3 check-history">
                <div className="card-header">
                    History
                </div>
                <div className="card-body row">
                    <VehiclesHistory/>
                    <RoutesHistory/>
                </div>
            </div>
        );
    }

}