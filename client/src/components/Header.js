import React from 'react';

export class Header extends React.Component {

    render() {
        return (
            <nav className="navbar navbar-light bg-light navbar-expand-md justify-content-between">
                <div className="container-fluid">
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target=".dual-nav">
                        <span className="navbar-toggler-icon"/>
                    </button>
                    <div className="navbar-collapse collapse dual-nav w-50 order-1 order-md-0">
                        <ul className="navbar-nav">
                            <li className="nav-item">
                                <a className="nav-link" href="/docs">Docs</a>
                            </li>
                        </ul>
                    </div>
                    <a href="/" className="navbar-brand mx-auto d-block text-center order-0 order-md-1 w-25 page-header">Prague Shared Traffic</a>
                    <div className="navbar-collapse collapse dual-nav w-50 order-2">
                        <ul className="nav navbar-nav ml-auto" />
                    </div>
                </div>
            </nav>
        );
    }

}