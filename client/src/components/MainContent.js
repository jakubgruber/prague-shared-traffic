import React from 'react';
import {TrafficMap} from "./TrafficMap";
import {RouteInfo} from "./routes/RouteInfo";
import {History} from "./History";
import {Statistics} from "./Statistics";
import {MapInfoProvider, RouteInfoProvider} from "../contextProvider";
import {SharedTrafficService} from "../service/SharedTrafficService";

export class MainContent extends React.Component {

    service = new SharedTrafficService();

    constructor(props) {
        super(props);

        this.state = {
            computeRoute: this.computeRoute,
            onDeleteVehicle: this.onDeleteVehicle,
            onDeleteRoute: this.onDeleteRoute,
            onMapLoaded: this.onMapLoaded,
            resetRouteInfo: this.resetRouteInfo,
            // routeInfo contexts
            routeInfo: undefined,
            routeItem: undefined,
            routePage: 0,
            changeRoutePage: this.changeRoutePage
        };
    }

    componentDidMount() {
        this.loadDbData();
    }

    computeRoute = async (vehicle, from) => {
        let routeInfo = await this.service.computeRoute(vehicle, from);
        let routePath;
        if (this.state.map && this.state.maps) {
            routePath = new this.state.maps.Polyline({
                path: routeInfo.itineraryItems.map((item) => item.place),
                geodesic: true,
                strokeColor: '#03a1fc',
                strokeOpacity: 1.0,
                strokeWeight: 3
            });

            routePath.setMap(this.state.map)
        }

        this.setState({
            routePath: routePath,
            routeInfo: routeInfo,
            routeItem: routeInfo.itineraryItems[0],
            routePage: 0
        });

        this.loadDbData();
    };

    loadDbData = async () => {
        let routeHistory = await this.service.getRoutesHistory();
        let routeStats = await this.service.getRoutesStats();
        let vehicleHistory = await this.service.getVehiclesHistory();
        let vehicleStats = await this.service.getVehiclesStats();

        console.log('Data loaded');

        this.setState({
            routeHistory: routeHistory,
            routeStats: routeStats,
            vehicleHistory: vehicleHistory,
            vehicleStats: vehicleStats
        })
    };

    onDeleteVehicle = async (vehicleId) => {
        await this.service.deleteVehicle(vehicleId);
        let vehicleHistory = await this.service.getVehiclesHistory();
        let vehicleStats = await this.service.getVehiclesStats();

        this.setState({
            vehicleHistory: vehicleHistory,
            vehicleStats: vehicleStats
        })
    };

    onDeleteRoute = async (routeId) => {
        await this.service.deleteRoute(routeId);
        let routeHistory = await this.service.getRoutesHistory();
        let routeStats = await this.service.getRoutesStats();

        this.setState({
            routeHistory: routeHistory,
            routeStats: routeStats
        })
    };

    onMapLoaded = (map, maps) => {
        this.setState({
            map: map,
            maps: maps
        })
    };

    resetRouteInfo = () => {
        if (this.state.routePath) {
            this.state.routePath.setMap(null);
        }

        this.setState({
            routePath: undefined,
            routeInfo: undefined
        });

    };

    changeRoutePage = (newRoutePage) => {
        if (newRoutePage >= 0 && newRoutePage < this.state.routeInfo.itineraryItems.length) {
            this.setState({
                routePage: newRoutePage,
                routeItem: this.state.routeInfo.itineraryItems[newRoutePage]
            });
        }
    };

    render() {
        return (
            <MapInfoProvider.Provider value={this.state}>
                <TrafficMap/>
                <RouteInfoProvider.Provider value={this.state}>
                    <RouteInfo/>
                </RouteInfoProvider.Provider>
                <History/>
                <Statistics/>
            </MapInfoProvider.Provider>
        );
    }

}