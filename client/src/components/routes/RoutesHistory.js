import React from 'react';
import {MapInfoProvider} from "../../contextProvider";

export class RoutesHistory extends React.Component {

    deleteRouteOnClick = (onDeleteRoute, id) => {
        onDeleteRoute(id);
    };

    render() {
        return (
            <MapInfoProvider.Consumer>
                {({routeHistory, onDeleteRoute}) => {
                    if(!routeHistory) routeHistory = [];
                    return (
                        <div className="col">
                            <h5 className="card-title">Route history</h5>
                            <ul className="list-group list-group-flush">
                                {
                                    routeHistory.map((route, key) => {
                                        let date = new Date(route.dateTaken);
                                        let dateTakenFormatted = date.getDate() + '.' + (date.getMonth() + 1) + '.' + date.getFullYear();

                                        return <li className="list-group-item" key={key}>
                                            <div className="card-body">
                                                <button type="button" className="close delete-history"
                                                        aria-label="Close"
                                                        onClick={e => this.deleteRouteOnClick(onDeleteRoute, route._id)}>
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                                <h6 className="card-subtitle mb-2 text-muted">{dateTakenFormatted}</h6>
                                                <p className="card-text">You took a {route.distance} km route
                                                    in {route.duration} minutes.</p>
                                            </div>
                                        </li>
                                    })
                                }
                            </ul>
                        </div>
                    );
                }}
            </MapInfoProvider.Consumer>
        );
    }

}