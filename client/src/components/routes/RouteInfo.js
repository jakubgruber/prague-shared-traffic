import React from 'react';
import {RouteInfoProvider} from "../../contextProvider";

export class RouteInfo extends React.Component {

    render() {
        return (
            <RouteInfoProvider.Consumer>
                {({routeInfo, routeItem, routePage, changeRoutePage}) => {
                    if (routeInfo) {
                        return (
                            <div className="mx-auto card w-75 my-3 check-instructions">
                                <div className="card-header">
                                    Route info
                                </div>
                                <div className="card-body">
                                    <h6 className="card-subtitle mb-2 text-muted">{routeInfo.distance} km</h6>
                                    <h6 className="card-subtitle mb-2 text-muted">{routeInfo.duration} minutes</h6>
                                </div>
                                <ul className="list-group list-group-flush">
                                    <li className="list-group-item">{routeItem.text}</li>
                                </ul>
                                <div className="card-footer">
                                    <ul className="pagination justify-content-center">
                                        {(routePage > 0) &&
                                        <li className="page-item"><a className="page-link"
                                                                     onClick={(e) => changeRoutePage(routePage - 1)}>Prev</a>
                                        </li>
                                        }
                                        <li className="page-item active"><span className="page-link">
                                            {routePage + 1} / {routeInfo.itineraryItems.length}
                                            <span className="sr-only">(current)</span>
                                          </span>
                                        </li>
                                        {(routePage < routeInfo.itineraryItems.length - 1) &&
                                        <li className="page-item"><a className="page-link"
                                                                     onClick={(e) => changeRoutePage(routePage + 1)}>Next</a>
                                        </li>
                                        }
                                    </ul>
                                </div>
                            </div>
                        );
                    } else {
                        return (
                            <div className="mx-auto card w-75 my-3 check-instructions">
                                <div className="card-header">
                                    Route info
                                </div>
                                <div className="card-body">
                                    <p className="card-text"><i>This card will display route info data once you choose
                                        bike.</i></p>
                                </div>
                            </div>
                        );
                    }
                }}
            </RouteInfoProvider.Consumer>
        );
    }

}