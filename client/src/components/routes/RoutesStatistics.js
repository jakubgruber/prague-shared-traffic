import React from 'react';
import {MapInfoProvider} from "../../contextProvider";

export class RoutesStatistics extends React.Component {

    render() {
        return (
            <MapInfoProvider.Consumer>
                {({routeStats}) => {
                    if (!routeStats || routeStats.length === 0) {
                        routeStats = {
                            totalDistance: 0,
                            totalDuration: 0,
                            lastTakenDate: 'never'
                        }
                    } else {
                        let date = new Date(routeStats.lastTakenDate);
                        routeStats.lastTakenDate = date.getDate() + '.' + (date.getMonth() + 1) + '.' + date.getFullYear();
                    }
                    return (
                        <div className="col">
                            <h5 className="card-title">Route statistics</h5>
                            <ul className="list-group list-group-flush">
                                <li className="list-group-item">Total distance
                                    travelled: {routeStats.totalDistance} km
                                </li>
                                <li className="list-group-item">Total duration
                                    spend: {routeStats.totalDuration} minutes
                                </li>
                                <li className="list-group-item">The service was last used
                                    on {routeStats.lastTakenDate}</li>
                            </ul>
                        </div>
                    );
                }}
            </MapInfoProvider.Consumer>
        );

    }

}