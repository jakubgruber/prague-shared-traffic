import React from 'react';
import {Header} from "./components/Header"
import {MainContent} from "./components/MainContent";
import Joyride from 'react-joyride';

export default class App extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            steps: [
                {
                    target: '.page-header',
                    content: 'Welcome to Prague Shared Traffic! The app let\'s you find the a route to a shared vehicle you choose.'
                },
                {
                    target: '.map-location',
                    content: 'Let the browser detect your location or click on map to choose on your own!',
                    placement: 'center'
                },
                {
                    target: '.goforit-button',
                    content: 'Once the bikes are loaded, choose one and "Go for it!"',
                },
                {
                    target: '.check-instructions',
                    content: 'You can find route instructions here.',
                },
                {
                    target: '.check-history',
                    content: 'Check what vehicles and routes you are using.',
                },
                {
                    target: '.delete-history',
                    content: 'Delete records you do not like.',
                },
                {
                    target: '.check-statistics',
                    content: 'Admire the statistics.',
                }
            ]
        };
    }

    render() {
        const {steps} = this.state;
        return (
            <div>
                <Header/>
                <MainContent/>
                <Joyride steps={steps} continuous={true} showSkipButton={true} showProgress={true}/>
            </div>
        );
    }

}
