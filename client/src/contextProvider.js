import React from 'react';

export const MapInfoProvider = React.createContext({
    computeRoute: async (vehicle, position) => {},
    routeHistory: [],
    routeStats: [],
    vehicleHistory: [],
    vehicleStats: [],
    onMapLoaded: (map) => {}
});

export const RouteInfoProvider = React.createContext({
    routeInfo: undefined,
    routeItem: undefined,
    routePage: 0,
    changeRoutePage: (routePage) => {}
});