const axios = require('axios');

export class SharedTrafficService {

    getRoutesHistory = async () => {
        let response = await axios.get('/routes/');
        return response.data;
    };

    deleteRoute = async (id) => {
        return await axios.delete('/routes/' + id);
    };

    computeRoute = async (vehicle, from) => {
        let response = await axios.post('/routes/compute', {
            vehicle: vehicle,
            from: from
        });
        return response.data;
    };

    getRoutesStats = async () => {
        let response = await axios.get('/routes/statistics');
        return response.data;
    };


    getVehiclesHistory = async () => {
        let response = await axios.get('/vehicles/');
        return response.data;
    };

    getVehiclesAround = async (lat, lng) => {
        const queryParams = new URLSearchParams();
        queryParams.append('latlng', lat + ',' + lng);

        let response = await axios.get('/vehicles/around', queryParams);
        return response.data;
    };

    deleteVehicle = async (id) => {
        return await axios.delete('/vehicles/' + id);
    };

    getVehiclesStats = async () => {
        let response = await axios.get('/vehicles/statistics');
        return response.data;
    };

}