# prague-shared-traffic

The application is going to serve to citizens of Prague that like to use shared transportation options like shared cars, bikes or scooters. It's going to provide a map of Prague and based on filtering criteria, find the nearest shared transportation option and recommend the route to take to destination.

`https://prague-shared-traffic.herokuapp.com`

## General Info

- What problem are you solving?

Routing to shared bicycles and scooters around Prague.

- Why is your idea unique? Who are the competitors? Why are you better? 

User's can use native apps provided by the sharing platform, but my app combines all of them together.

- What are the steps to achieve your goals?

Combine multiple apis to build the final app.
_https://golemioapi.docs.apiary.io/#reference/traffic_
_https://docs.microsoft.com/en-us/bingmaps/rest-services_

- Your customers are students, how large is the target audience?

Anyone who likes to travel ecologically.

- Quantify benefits of your product (cheaper, faster, other product improvements) 

Combining all together, storing history, displaying statistics.


## Progress

> [Development progress](https://gitlab.com/jakubgruber/prague-shared-traffic/-/wikis/home)

## Use cases

### Find vehicle to use
- load current position / pick one by clicking on map
- load all bikes around `/vehicles/around`
- choose bike by clicking on it 
- click "Go for it!"
- compute route to bike `/routes/compute`
    - save vehicle to history
    - save route to history
- show instructions

### View history
- see history for all vehicles
- possibility to remove entries

### Remove from history
- remove vehicle/route from history

### View routes & vehicles statistics
- display simple statistics for routes and vehicles